﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">37887</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)]!!!*Q(C=\&gt;5^&lt;NN!%-8RFS"&amp;7F6J!VVBA*R!L3O$=/^CLK!WJ?JU\QI]A!/9D1YQL5N&gt;16?1`VSN*;7R'DM)!O^S^@()H@W:J'CJN;`3&amp;RWON?WL\8!1LTJOUP;Y&lt;5]&lt;_Y`B^C)^^T;`^:&gt;:,`.&lt;M80^V_&lt;`M@^S,V_O_[]U@&lt;30^H_XHV&gt;`"/`_/`CE_5(%+)9:S1D.SR[+P-C,P-C,P-B.&lt;H+4G^TE*E`S*%`S*%`S*!`S)!`S)!`S)'_&gt;8/1C&amp;TGE9P&amp;CI;*I5;!Y')K+F]*4?!J0Y?'L#E`B+4S&amp;J`"QC!J0Y3E]B;@Q-%W&amp;J`!5HM*4?#D6*.5[/:\#1XE:D`%9D`%9$UP+?!T!,'9+GS)Q:(;;$]:D0-&lt;$2RG0]2C0]2A0OW5]RG-]RG-]4'FHR5UT&gt;X)]F&amp;(C34S**`%E(EIL]33?R*.Y%A`,+@%EHA32,*A5B["E5H*!]C8R*"\?F(A34_**0)G(8?U+:4MTMW&lt;OZ(A#4_!*0)%H]&amp;"#A3@Q"*\!%XAIK]!4?!*0Y!E],+8!%XA#4Q!*&amp;G6Z"=7#C=&amp;"12"YW.L&gt;%OUKO5GC&gt;;H@P/IXJ@L.JHY4K&gt;]=[B&gt;&gt;`7+K8S4VE[^_5N60FPJ*50`DV.(K'06&amp;V#@0"WL0[YYR-5&lt;'BL&amp;G$)Q69]F9T&amp;0@_-$^@K`&gt;&lt;K&gt;JGD3/IT;&lt;D&gt;&lt;LN9:BU'KVUH+ZV'+R/$U'PN.0$Y4T=WG[P`H^[_(J&lt;PRR]_W2^]?(Y@9P`8`_"Z[._KT,.4B(TY9/@X%!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Components" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Arc.lvclass" Type="LVClass" URL="../Components/Arc/Arc.lvclass"/>
		<Item Name="Circle.lvclass" Type="LVClass" URL="../Components/Circle/Circle.lvclass"/>
		<Item Name="Image.lvclass" Type="LVClass" URL="../Components/Image/Image.lvclass"/>
		<Item Name="Line.lvclass" Type="LVClass" URL="../Components/Line/Line.lvclass"/>
		<Item Name="Oval.lvclass" Type="LVClass" URL="../Components/Oval/Oval.lvclass"/>
		<Item Name="Point.lvclass" Type="LVClass" URL="../Components/Point/Point.lvclass"/>
		<Item Name="Rectangle.lvclass" Type="LVClass" URL="../Components/Rectangle/Rectangle.lvclass"/>
		<Item Name="Rounded Rectangle.lvclass" Type="LVClass" URL="../Components/Rounded Rectangle/Rounded Rectangle.lvclass"/>
		<Item Name="Text.lvclass" Type="LVClass" URL="../Components/Text/Text.lvclass"/>
	</Item>
	<Item Name="2D Picture.lvclass" Type="LVClass" URL="../2D Picture/2D Picture.lvclass"/>
	<Item Name="I2D Picture Component.lvclass" Type="LVClass" URL="../I2D Picture Component/I2D Picture Component.lvclass"/>
</Library>
